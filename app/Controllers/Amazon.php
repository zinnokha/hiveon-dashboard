<?php
namespace App\Controllers;
use CodeIgniter\Controller;

class Amazon extends Controller
{

protected $helpers = [''];

public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
{
	parent::initController($request, $response, $logger);
	$this->db = \Config\Database::connect('hiveon');
	$this->db = db_connect();;
}


public function index()
{

}

function clean($string) {
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

public function price_check()
{

	

	include_once('simple_html_dom.php');


	// Turn this into a DB later...
	$lookup_products = array(

		// array(
		// 	'name' => 'Logitech M510 TEST',
		// 	'asin' => 'B087Z5WDJ2',
		// 	'price_limit' => 25,
		// ),

		// array(
		// 	'name' => 'EVGA 08G-P5-3767-KR GeForce RTX 3070 FTW3 Ultra',
		// 	'asin' => 'B08L8L9TCZ',
		// 	'price_limit' => 750,
		// ),

		// array(
		// 	'name' => 'EVGA 08G-P5-3755-KR GeForce RTX 3070 XC3 Ultra',
		// 	'asin' => 'B08L8L71SM',
		// 	'price_limit' => 750,
		// ),

		// array(
		// 	'name' => 'ASUS TUF Gaming NVIDIA GeForce RTX 3070 OC',
		// 	'asin' => 'B08L8KC1J7',
		// 	'price_limit' => 750,
		// ),

		// array(
		// 	'name' => 'GIGABYTE GeForce RTX 3070 Vision',
		// 	'asin' => 'B08M13DXSZ',
		// 	'price_limit' => 750,
		// ),


		// array(
		// 	'name' => 'EVGA 10G-P5-3897-KR GeForce RTX 3080 FTW3',
		// 	'asin' => 'B08HR3Y5GQ',
		// 	'price_limit' => 1000,
		// ),
		// array(
		// 	'name' => 'GIGABYTE GeForce RTX 3080 Vision',
		// 	'asin' => 'B08KGZVKXM',
		// 	'price_limit' => 1000,
		// ),
		array(
			'name' => 'GIGABYTE 3060',
			'asin' => 'B08NYP7KG6',
			'price_limit' => 600,
		),




	);

	foreach($lookup_products as $p){

		$html = file_get_html('https://www.amazon.com/gp/aod/ajax/ref=dp_aod_afts?asin='.$p['asin'].'&m=&pinnedofferhash=&qid=&smid=&sourcecustomerorglistid=&sourcecustomerorglistitemid=&sr=&pc=dp');

		$count = 0;

		// Pinned Prime option
		foreach($html->find('#aod-sticky-pinned-container') as $item){

			// If there is a pinned option
			if($item->find('.a-offscreen'))
			{
				$data[$count]['name'] = $p['name'];
				$data[$count]['asin'] = $p['asin'];
				$data[$count]['price_limit'] = $p['price_limit'];


				$data[$count]['vendor'] ='';
				foreach($item->find('.a-size-small') as $v){
					$data[$count]['vendor'] .= $this->clean(filter_var($v->plaintext,FILTER_SANITIZE_STRING)) . ' ';
				}
				
				foreach($item->find('.a-offscreen') as $v){
					$data[$count]['price'] = filter_var($v->plaintext, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
				}

				$count++;
			}
		}

		// All other items
		foreach($html->find('#aod-offer') as $item){

			$data[$count]['name'] = $p['name'];
			$data[$count]['asin'] = $p['asin'];
			$data[$count]['price_limit'] = $p['price_limit'];


			foreach($item->find('.a-link-normal') as $v){
				$data[$count]['vendor'] = 'Sold by ' . $this->clean(filter_var($v->plaintext,FILTER_SANITIZE_STRING));
			}
			


			foreach($item->find('.a-offscreen') as $v){
				$data[$count]['price'] = filter_var($v->plaintext, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
			}


			$count++;
		}


		foreach($data as $k => $v)
		{

			//Price Chhecker and emailer
			if($v['price_limit'] > $v['price']){

				$mail_data['From'] = ("info@ivtecc.com");
				$mail_data['To'] = ("5855208939@mms.att.net");
				// $mail_data['To'] = ("5855208939@mms.att.net,5857480272@mms.att.net");
				$mail_data['Subject'] = ($v['name']);
				$mail_data['TextBody'] = ("$" . number_format($v['price'],2) . " - " . $v['vendor'] . "

Product Link:
https://www.amazon.com/dp/". $v['asin'] . "

AOD Frame Link:
https://www.amazon.com/gp/aod/ajax/ref=dp_aod_afts?asin=" . $v['asin'] );

				$headers = array(
				'Accept: application/json',
				'Content-Type: application/json',
				'X-Postmark-Server-Token: b7c1bfea-372a-4671-97d1-7306799ca088'
				);

				$ch = curl_init('https://api.postmarkapp.com/email');

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($mail_data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

				$return = curl_exec($ch);
				$curl_error = curl_error($ch);
				$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);


				if($http_code == 200)
					$data[$k]['email_sent'] = 1;
				else
					$data[$k]['email_sent'] = 0;

				$data[$k]['price_limit_met'] = 1;
			}
			else{

					//Unset this array if not met? Save on DB resources.
					//unset($data[$k]);

					$data[$k]['email_sent'] = 0;
					$data[$k]['price_limit_met'] = 0;
			}

		}

		// print_r($data);die();

		$builder = $this->db->table('price_check_amazon');
		$builder->ignore(true)->insertBatch($data);


	}

	$output['status'] = 'success';
	echo json_encode($output);
	die();
}


}
