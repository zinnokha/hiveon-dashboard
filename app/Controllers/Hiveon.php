<?php
namespace App\Controllers;
use CodeIgniter\Controller;

class Hiveon extends Controller
{

protected $helpers = [''];

public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
{
	parent::initController($request, $response, $logger);
	$this->db = \Config\Database::connect('hiveon');
	$this->db = db_connect();;
}


public function index()
{

}

public function view($request)
{


	$request = filter_var($request, FILTER_SANITIZE_STRING);
	$sql = "SELECT * FROM hiveon_users WHERE address = '".$request."'";
	$results = $this->db->query($sql)->getResult();

	if(count($results) > 0){
			$user_id = $results[0]->id;
	}
	else{
		$error['status'] = 'Error';
		$error['message'] = 'Sorry no address found';
		print_r($error);
		die();
	}

	$data['address'] = $request;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/pool');
	$pool = json_decode(curl_exec($ch));
	curl_close($ch);





		$hiveon['timestamp'] = date('Y-m-d H:i:s');
		$hiveon['user_id'] = $user_id;

		if(isset($pool->stats->ETH->exchangeRates->USD))
			$hiveon['eth_usd'] = $pool->stats->ETH->exchangeRates->USD;

		if(isset($pool->stats->ETH->expectedReward24H))
			$hiveon['expected_100mh'] = $pool->stats->ETH->expectedReward24H;


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH/billing-acc');
		$billing = json_decode(curl_exec($ch));
		curl_close($ch);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH');
		$eth = json_decode(curl_exec($ch));
		curl_close($ch);

		if(isset($billing->totalUnpaid))
			$hiveon['unpaid'] = $billing->totalUnpaid;

		// If there is no hash rate, just skip everything!
		if(isset($eth->hashrate) && isset($eth->reportedHashrate)){

			$hiveon['hiveon_hashrate'] = ($eth->hashrate * .000001);

			$hiveon['reported_hashrate'] = ($eth->reportedHashrate * .000001);

			if(isset($eth->onlineWorkerCount))
				$hiveon['online_workers'] = $eth->onlineWorkerCount;

			if(isset($eth->offlineWorkerCount))
				$hiveon['offline_workers'] = $eth->offlineWorkerCount;

			if(isset($eth->sharesStatusStats->validCount))
				$hiveon['valid_shares'] = $eth->sharesStatusStats->validCount;

			if(isset($eth->sharesStatusStats->staleCount))
				$hiveon['stale_shares'] = $eth->sharesStatusStats->staleCount;

			if(isset($eth->sharesStatusStats->validRate) AND $eth->sharesStatusStats->validRate != 'NaN')
				$hiveon['valid_rate'] = $eth->sharesStatusStats->validRate;

			if(isset($billing->expectedReward24H))
				$hiveon['expected_all_mh'] = $billing->expectedReward24H;

			$output[$user_id] = 'success';
			$hiveon['valid_entry'] = 1;

		}
		else{

			if(isset($eth->offlineWorkerCount))
				$hiveon['offline_workers'] = $eth->offlineWorkerCount;

			$output[$user_id] = 'invalid_entry';
			$hiveon['valid_entry'] = 0;
		}




		$data['hiveon'] = $hiveon;

		$data['eth_usd'] = $hiveon['eth_usd'];


	/*


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/pool');
	$pool = json_decode(curl_exec($ch));
	curl_close($ch);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH/billing-acc');
	$billing = json_decode(curl_exec($ch));
	curl_close($ch);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH');
	$eth = json_decode(curl_exec($ch));
	curl_close($ch);


	
	// print_r($eth);

	$data['address'] = $request;

	//$hiveon['timestamp'] = date('Y-m-d H:i:s');

	if(isset($pool->stats->ETH->exchangeRates->USD))
		$hiveon['eth_value'] = $eth_usd = $pool->stats->ETH->exchangeRates->USD;
	else{
		$hiveon['eth_value'] = 'ERR';
		$eth_usd = 0;
	}

	if(isset($billing->totalUnpaid)){
		$hiveon['unpaid_eth'] = round($billing->totalUnpaid,8);
		$hiveon['unpaid_usd'] = round(($billing->totalUnpaid * $eth_usd),2);
	}
	else{
		$hiveon['unpaid_eth'] = 'ERR';
		$hiveon['unpaid_usd'] = 'ERR';
	}

	if(isset($eth->hashrate))
		$hiveon['hiveon_hashrate'] = round(($eth->hashrate * .000001),2);
	else
		$hiveon['hiveon_hashrate'] = 'ERR';
	
	if(isset($eth->reportedHashrate))
		$hiveon['reported_hashrate'] = round(($eth->reportedHashrate * .000001),2);
	else
		$hiveon['reported_hashrate'] = 'ERR';


	if(isset($eth->onlineWorkerCount))
		$hiveon['workers'] = $eth->onlineWorkerCount;
	else
		$hiveon['workers'] = 'ERR';


	if(isset($eth->sharesStatusStats->validCount))
		$hiveon['valid_shares'] = $eth->sharesStatusStats->validCount;
	else
		$hiveon['valid_shares'] = 'ERR';

	if(isset($eth->sharesStatusStats->staleCount))
		$hiveon['stale_shares'] = $eth->sharesStatusStats->staleCount;
	else
		$hiveon['stale_shares'] = 'ERR';

	if(isset($eth->sharesStatusStats->validRate) AND $eth->sharesStatusStats->validRate != 'NaN')
		$hiveon['valid_rate'] = round($eth->sharesStatusStats->validRate,2);
	else
		$hiveon['valid_rate'] = 'ERR';

	if(isset($billing->expectedReward24H))
		$hiveon['expected_all_mh'] = $billing->expectedReward24H;
	else
		$hiveon['expected_all_mh'] = 'ERR';

	if(isset($pool->stats->ETH->expectedReward24H))
		$hiveon['expected_100mh'] = round($pool->stats->ETH->expectedReward24H,8);
	else
		$hiveon['expected_100mh'] = 'ERR';

	$data['hiveon'] = $hiveon;

	*/

	// Find todays value
	$sql = "
	SELECT
		date(hiveon.timestamp) as date,
		AVG(hiveon.online_workers) as workers_avg,
		AVG(hiveon.reported_hashrate) as reported_hashrate_avg,
		AVG(hiveon.hiveon_hashrate) as hiveon_hashrate_avg,
		AVG(hiveon.eth_usd) as eth_usd_avg,
		AVG(hiveon.expected_100mh) as expected_100mh_avg,
		MIN(hiveon.id) as min_id, 
		MAX(hiveon.id) as max_id,
		min.id as start_id,
		max.id as end_id,
		min.unpaid as start_unpaid,
		max.unpaid as end_unpaid
	FROM hiveon 
	JOIN ( SELECT id, unpaid, date(hiveon.timestamp) AS date FROM hiveon WHERE id IN ( SELECT MIN(id) FROM hiveon WHERE user_id = ".$user_id." AND pool = 'hiveon' GROUP BY date(hiveon.timestamp) ) ) AS min ON min.date = date(hiveon.timestamp)
	JOIN ( SELECT id, unpaid, date(hiveon.timestamp) AS date FROM hiveon WHERE id IN ( SELECT MAX(id) FROM hiveon WHERE user_id = ".$user_id." AND pool = 'hiveon'  GROUP BY date(hiveon.timestamp) ) ) AS max ON max.date = date(hiveon.timestamp)
	WHERE hiveon.user_id = ".$user_id." 
	GROUP BY date ORDER BY date DESC
	";

	$results = $this->db->query($sql)->getResult();

	// print_r($results);die();

	foreach($results as $row) {



		if($row->end_unpaid < $row->start_unpaid){
			$total = $row->end_unpaid;
			$payout = 1;
		}
		else{
			$total = $row->end_unpaid - $row->start_unpaid;
			$payout = 0;
		}

		$data['history'][$row->date]['eth'] = round($total,8);
		$data['history'][$row->date]['eth_balance'] = $row->end_unpaid;
		$data['history'][$row->date]['payout'] = $payout;
		$data['history'][$row->date]['eth_day_avg'] = round($row->eth_usd_avg,2);
		$data['history'][$row->date]['workers_avg'] = round($row->workers_avg,2);
		$data['history'][$row->date]['hiveon_hashrate_avg'] = round($row->hiveon_hashrate_avg,2);
		$data['history'][$row->date]['reported_hashrate_avg'] = round($row->reported_hashrate_avg,2);
		$data['history'][$row->date]['expected_100mh_avg'] = round($row->expected_100mh_avg,8);

	}

	// print_r($data);die();


	return view('hiveon_dash', $data);

}



public function view_dev($request)
{

	// Find user
	// $request = '0f9f803db95edb62f2c7eeb1be8b1eeb3d819ca6';
	$request = filter_var($request, FILTER_SANITIZE_STRING);
	$sql = "SELECT * FROM hiveon_users WHERE address = '".$request."'";
	$results = $this->db->query($sql)->getResult();

	if(count($results) > 0){
			$user_id = $results[0]->id;
	}
	else{
		$error['status'] = 'Error';
		$error['message'] = 'Sorry no address found';
		print_r($error);
		die();
	}

	

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/pool');
	$pool = json_decode(curl_exec($ch));
	curl_close($ch);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH/billing-acc');
	$billing = json_decode(curl_exec($ch));
	curl_close($ch);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH');
	$eth = json_decode(curl_exec($ch));
	curl_close($ch);

	echo "<pre>";
	echo '<br/><br/>POOL<br/>';
	print_r($pool);
	echo '<br/><br/>BILLING<br/>';
	print_r($billing);
	echo '<br/><br/>ETH<br/>';
	print_r($eth);

}


public function view_date($request,$request_date)
{

	// Find user
	// $request = '0f9f803db95edb62f2c7eeb1be8b1eeb3d819ca6';
	$request = filter_var($request, FILTER_SANITIZE_STRING);
	$sql = "SELECT * FROM hiveon_users WHERE address = '".$request."'";
	$results = $this->db->query($sql)->getResult();

	if(count($results) > 0){
			$user_id = $results[0]->id;
	}
	else{
		$error['status'] = 'Error';
		$error['message'] = 'Sorry no address found';
		print_r($error);
		die();
	}

	

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/pool');
	$pool = json_decode(curl_exec($ch));
	curl_close($ch);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH/billing-acc');
	$billing = json_decode(curl_exec($ch));
	curl_close($ch);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH');
	$eth = json_decode(curl_exec($ch));
	curl_close($ch);

	// print_r($eth);

	$data['address'] = $request;

	//$hiveon['timestamp'] = date('Y-m-d H:i:s');

	if(isset($pool->stats->ETH->exchangeRates->USD))
		$hiveon['eth_value'] = $eth_usd = $pool->stats->ETH->exchangeRates->USD;
	else{
		$hiveon['eth_value'] = 'ERR';
		$eth_usd = 0;
	}

	if(isset($billing->totalUnpaid)){
		$hiveon['unpaid_eth'] = round($billing->totalUnpaid,8);
		$hiveon['unpaid_usd'] = round(($billing->totalUnpaid * $eth_usd),2);
	}
	else{
		$hiveon['unpaid_eth'] = 'ERR';
		$hiveon['unpaid_usd'] = 'ERR';
	}

	if(isset($eth->hashrate))
		$hiveon['hiveon_hashrate'] = round(($eth->hashrate * .000001),2);
	else
		$hiveon['hiveon_hashrate'] = 'ERR';
	
	if(isset($eth->reportedHashrate))
		$hiveon['reported_hashrate'] = round(($eth->reportedHashrate * .000001),2);
	else
		$hiveon['reported_hashrate'] = 'ERR';


	if(isset($eth->onlineWorkerCount))
		$hiveon['workers'] = $eth->onlineWorkerCount;
	else
		$hiveon['workers'] = 'ERR';


	if(isset($eth->sharesStatusStats->validCount))
		$hiveon['valid_shares'] = $eth->sharesStatusStats->validCount;
	else
		$hiveon['valid_shares'] = 'ERR';

	if(isset($eth->sharesStatusStats->staleCount))
		$hiveon['stale_shares'] = $eth->sharesStatusStats->staleCount;
	else
		$hiveon['stale_shares'] = 'ERR';

	if(isset($eth->sharesStatusStats->validRate) AND $eth->sharesStatusStats->validRate != 'NaN')
		$hiveon['valid_rate'] = round($eth->sharesStatusStats->validRate,2);
	else
		$hiveon['valid_rate'] = 'ERR';

	if(isset($billing->expectedReward24H))
		$hiveon['expected_all_mh'] = $billing->expectedReward24H;
	else
		$hiveon['expected_all_mh'] = 'ERR';

	if(isset($pool->stats->ETH->expectedReward24H))
		$hiveon['expected_100mh'] = round($pool->stats->ETH->expectedReward24H,8);
	else
		$hiveon['expected_100mh'] = 'ERR';

	$data['hiveon'] = $hiveon;

	// Find todays value
	$sql = "
	SELECT
		*
	FROM hiveon 
	WHERE user_id = ".$user_id." AND timestamp LIKE '%".$request_date."%'
	";

	$results = $this->db->query($sql)->getResult();

	// print_r($results);die();

	$count = 0;
	$past_value = 0;
	foreach($results as $row) {

		if($past_value == 0){
			$eth = 0;
			$past_value = $row->unpaid;
		}
		else{
			$eth = $row->unpaid - $past_value;
			$past_value = $row->unpaid;
		}
		
		$data['history'][$count]['timestamp'] = $row->timestamp;
		$data['history'][$count]['eth_balance'] = $row->unpaid;
		$data['history'][$count]['eth'] = $eth;
		$data['history'][$count]['hiveon_hashrate'] = $row->hiveon_hashrate;
		$data['history'][$count]['reported_hashrate'] = $row->reported_hashrate;
		$data['history'][$count]['workers'] = $row->workers;
		$data['history'][$count]['valid_shares'] = $row->valid_shares;
		$data['history'][$count]['stale_shares'] = $row->stale_shares;
		$data['history'][$count]['valid_rate'] = $row->valid_rate;
		$data['history'][$count]['expected_100mh'] = $row->expected_100mh;
		$data['history'][$count]['eth_usd'] = $row->eth_usd;

		$count ++;

	}

	// print_r($data['history']);die();

	return view('hiveon_view_date', $data);

}







}
