<?php
namespace App\Controllers;
use CodeIgniter\Controller;

class Hiveon_api extends Controller
{

protected $helpers = [''];

public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
{
	parent::initController($request, $response, $logger);
	$this->db = \Config\Database::connect('hiveon');
	$this->db = db_connect();;
}


public function index()
{

}

public function import()
{


	$sql = "SELECT * FROM  hiveon_users;";
	$results = $this->db->query($sql)->getResult();

	if(count($results) == 0){
		$output['status'] = 'error';
		$output['message'] = 'no users found';
		echo json_encode($output);
		die();
	}


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/pool');
	$pool = json_decode(curl_exec($ch));
	curl_close($ch);



	foreach($results as $user) {

		$user_id = $user->id;
	    $request = $user->address;

	    // Reset everything
	    $hiveon = NULL;
	    $billing = NULL;
	    $eth = NULL;

		$hiveon['timestamp'] = date('Y-m-d H:i:s');
		$hiveon['user_id'] = $user_id;
		$hiveon['pool'] = 'hiveon';

		if(isset($pool->stats->ETH->exchangeRates->USD))
			$hiveon['eth_usd'] = $pool->stats->ETH->exchangeRates->USD;

		if(isset($pool->stats->ETH->expectedReward24H))
			$hiveon['expected_100mh'] = $pool->stats->ETH->expectedReward24H;


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH/billing-acc');
		$billing = json_decode(curl_exec($ch));
		curl_close($ch);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://hiveon.net/api/v1/stats/miner/'.$request.'/ETH');
		$eth = json_decode(curl_exec($ch));
		curl_close($ch);

		if(isset($billing->totalUnpaid))
			$hiveon['unpaid'] = $billing->totalUnpaid;

		// If there is no hash rate, just skip everything!
		if(isset($eth->hashrate) && isset($eth->reportedHashrate)){

			$hiveon['valid_entry'] = 1;

			$hiveon['hiveon_hashrate'] = ($eth->hashrate * .000001);

			$hiveon['reported_hashrate'] = ($eth->reportedHashrate * .000001);

			if(isset($eth->onlineWorkerCount))
				$hiveon['online_workers'] = $eth->onlineWorkerCount;

			if(isset($eth->offlineWorkerCount))
				$hiveon['offline_workers'] = $eth->offlineWorkerCount;

			if(isset($eth->sharesStatusStats->validCount))
				$hiveon['valid_shares'] = $eth->sharesStatusStats->validCount;

			if(isset($eth->sharesStatusStats->staleCount))
				$hiveon['stale_shares'] = $eth->sharesStatusStats->staleCount;

			if(isset($eth->sharesStatusStats->validRate) AND $eth->sharesStatusStats->validRate != 'NaN')
				$hiveon['valid_rate'] = $eth->sharesStatusStats->validRate;

			$builder = $this->db->table('hiveon');
			$builder->ignore(true)->insert($hiveon);

			$output[$user_id] = 'hiveon';


		}
		else{

			// if(isset($eth->offlineWorkerCount))
			// 	$hiveon['offline_workers'] = $eth->offlineWorkerCount;

			// $output[$user_id] = 'invalid_entry';
			$hiveon['valid_entry'] = 0;
		}
		
		// print_r($hiveon);die();



		//Query Ethermine
		if($hiveon['valid_entry'] == 0){

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, 'https://api.ethermine.org/miner/'.strtoupper($request).'/dashboard');
			$ethermine = json_decode(curl_exec($ch));
			curl_close($ch);

			if(isset($ethermine->data->currentStatistics->currentHashrate) && isset($ethermine->data->currentStatistics->reportedHashrate)){


				$em['timestamp'] = date('Y-m-d H:i:s');
				$em['user_id'] = $user_id;
				$em['pool'] = 'ethermine';

				$em['valid_entry'] = 1;

				if(isset($pool->stats->ETH->exchangeRates->USD))
					$em['eth_usd'] = $pool->stats->ETH->exchangeRates->USD;

				if(isset($pool->stats->ETH->expectedReward24H))
					$em['expected_100mh'] = $pool->stats->ETH->expectedReward24H;



				$ethermine_data = $ethermine->data->currentStatistics;

				$em['hiveon_hashrate'] = ($ethermine_data->currentHashrate * .000001);

				$em['reported_hashrate'] = ($ethermine_data->reportedHashrate * .000001);

				if(isset($ethermine_data->unpaid))
					$em['unpaid'] = ($ethermine_data->unpaid * .000000000000000001);

				if(isset($ethermine_data->activeWorkers))
					$em['online_workers'] = $ethermine_data->activeWorkers;

				if(isset($ethermine_data->validShares))
					$em['valid_shares'] = $ethermine_data->validShares;

				if(isset($ethermine_data->staleShares))
					$em['stale_shares'] = $ethermine_data->staleShares;

				if(isset($ethermine_data->validShares) && isset($ethermine_data->staleShares))
					$em['valid_rate'] = ($ethermine_data->validShares / ($ethermine_data->staleShares + $ethermine_data->validShares)) * 100;


				$builder = $this->db->table('hiveon');
				$builder->ignore(true)->insert($em);

				$output[$user_id] = 'ethermine';

			}

		


		}


	}

	echo json_encode($output);
	die();


}





}
